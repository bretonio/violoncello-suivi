import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import Bach from '../views/Bach.vue'
import Telemann from '../views/Telemann.vue'
import Gongora from '../views/Gongora.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/bach',
    name: 'Bach',
    component: Bach
  },
  {
    path: '/telemann',
    name: 'Telemann',
    component: Telemann
  },
  {
    path: '/gongora',
    name: 'Gongora',
    component: Gongora
  },
  {
    path: '/about',
    name: 'About',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/About.vue')
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
